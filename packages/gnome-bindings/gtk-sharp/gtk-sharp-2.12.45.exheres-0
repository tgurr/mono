# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require mono autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.11 ] ]

SUMMARY="C# bindings for GTK+"
HOMEPAGE="https://www.mono-project.com/docs/gui/gtksharp/"

DOWNLOADS="https://download.mono-project.com/sources/${PN}$(ever delete_all $(ever range 1-2))/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        dev-lang/mono[>=1.0][X]
        dev-libs/atk
        dev-libs/glib:2[>=2.12]
        x11-libs/pango
        x11-libs/gtk+:2[>=2.12]
        gnome-platform/libglade:2[>=2.3.6]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-fix-libdir.patch
    "${FILES}"/${PN}-mono-ambiguous-range.patch
)

pkg_setup() {
    export CSC=/usr/$(exhost --target)/bin/mcs
}

src_prepare() {
    default

    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)&:g" configure.in
    edo sed \
            -e "/^prefix/s:=.*:=/usr:" \
            -e "/^exec_prefix/s:=.*:=/usr/$(exhost --target):" \
            -i parser/gapi-2.0.pc.in \
            -i gtk/gtk-sharp-2.0.pc.in \
            -i glade/glade-sharp-2.0.pc.in \
            -i glib/glib-sharp-2.0.pc.in \
            -i gtkdotnet/gtk-dotnet-2.0.pc.in

    eautoreconf
}

